def fileExamples():
    """Various examples of ways you could use a list comprehension with
    something more elaborate, in this case file I/O.  This example will not
    scale well, as it will build the entire list at once, which will load the
    entire file at once without checking how large it is first."""
    with open('input-test.txt', 'rt') as fIn:
        print([x.strip() for x in fIn if x.strip() != ''])

    """This is an example that will do the same basic thing, but without the
    list comprehension."""
    with open('input-test.txt', 'rt') as fIn:
        for x in fIn:
            x = x.strip()
            if x == '':
                continue
            print(x)

    """Here is a hybrid solution that uses the list comprehension for easy
    filtering.  This is done basically to show a what-not-to-do.  This is
    basically just wasting memory to save one line of code.  It isn't
    faster than the above, and it potentially uses a lot more memory."""
    with open('input-test.txt', 'rt') as fIn:
        for x in [x.strip() for x in fIn]:
            if x == '':
                continue
            print(x)

import random
from functools import reduce

def addtwo( a, b ):
    """Helper function to add two numbers, an alternative to a lambda"""
    return a+b

def stat( lst ):
    """Example function to compute some basic stats on a list of numbers."""
    # Easy, built-in way to get the sum of a list of numbers
    #sumRes = sum(lst)
    
    # Using the reduce function with a function that will add two numbers
    #sumRes = reduce( addtwo, lst )

    # The same as above, but using an inline, lambda function instead of a
    # traditionally declared, named function
    sumRes = reduce( lambda a, b: a+b, lst )
    avr = sumRes/len(lst)
    return (len(lst),sumRes, avr)

def normRand( iterCount ):
    """This is an example of how to turn a complex comprehension/lambda function
    into an easier to read and re-use function."""
    # The original, scary looking function call:
    # reduce( lambda a, b: a+b, [random.random() for i in range(3)] )/3.0

    # Instead we use a for loop and a collection variable
    sumRes = 0.0
    for i in range(iterCount):
        sumRes = sumRes + random.random()

    # Return the average
    return sumRes/iterCount

def isMiddleValue( x ):
    return x >= .3333 and x <= .6666

def filtering():
    # Bad approach (in that this is very hard to understand)
    #samples = [reduce( lambda a, b: a+b, [random.random() for i in range(3)] )/3.0 for x in range(100)]
    
    # A better approach, break the inner comprehension up and make it a,
    # function, in this case normRand
    samples = [normRand(3) for x in range(100)]
    
    # Filter the list down
    median = [x for x in samples if isMiddleValue(x)]

    print(stat(samples))
    print(stat(median))

filtering()
    
    
