
# Get the name of the user
name = input('Enter your name: ')

age = input('Enter your age: ')
age = int(age)

print(type(age))

print(f'Hello {name}')
print(f'You are {age} years old.')
print('In 10 years you will be %d, which is %f percent older.' % (age+10, 10*100/age))

# == > < >= <= !=

if age >= 21:
    print('You are old enough to drink (in the US)')
    print('Something else')
elif age >= 18:
    print('You are old enough to smoke (in the US)')
elif age >= 16:
    print('You are old enough to get a learner\'s permit (in the US)')
else:
    print('You are not old enough to do anything fun (in the US)')

