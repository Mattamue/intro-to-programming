"""
Functions and utilities for working with playing cards.
"""

if __name__ == '__main__':
    raise Exception('Cannot run this file directly.')

def generateDeck( suits, ranks ):
    """
    Combines suits and ranks to produce a full deck of playing cards, returned
    as a list.
    """
    deck = []
    for suit in suits:
        for rank in ranks:
            deck.append( rank + suit )
    return deck

standardSuits = ['H','C','S','D']

print('cards/helper.py has been loaded: ' + __name__)
