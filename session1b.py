
import random

target = random.randint(0, 100)
guess = None

while target != guess:
    guess = input('Enter your guess: ')
    guess = int(guess)

    if target < 0 or target > 100:
        print('The valid range is 0-100')

    if guess == target:
        print('You guessed it!')
    elif guess < target:
        print('You guessed low.')
    elif guess > target:
        print('You guessed high.')
    else:
        print('You entered something I didn\'t understand.')
