if __name__ == "__main__":
    raise Exception('Do not run this file directly.')

from .floor import Floor
from .cell import Cell
from .player import Player

class GameState():
    def __init__( self ):
        self.floor_list = []
        self.floor = 0
        self.floor_list.append( Floor() )

        # Test code in place of using a mapper
        self.floor_list[0].grow( 5, 5, 5, 5 )
        for y in range( -5, 5 ):
            for x in range( -5, 5 ):
                self.floor_list[0].set( x, y, Cell() )

        self.player = Player()
        self.player.set_floor( self.floor_list[0], 0, 0 )

    def on_input( self, key ):
        if key == 'Q' or key == 'Escape':
            self.ui.exit()
        elif key == '8' or key == 'Up':
            self.move_player( 0, -1 )
        elif key == '2' or key == 'Down':
            self.move_player( 0, 1 )
        elif key == '4' or key == 'Left':
            self.move_player( -1, 0 )
        elif key == '6' or key == 'Right':
            self.move_player( 1, 0 )

        # Timestep the world

    def move_player( self, delta_x, delta_y ):
        floor, new_x, new_y = self.player.get_location()
        new_x = new_x + delta_x
        new_y = new_y + delta_y
        if not floor.is_in_bounds( new_x, new_y ):
            return
        self.player.set_location( new_x, new_y )

    def render( self, x, y, width, height ):
        return self.floor_list[self.floor].render( x, y, width, height )

