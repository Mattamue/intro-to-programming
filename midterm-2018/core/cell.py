if __name__ == "__main__":
    raise Exception('Do not run this file directly.')

class Cell():
    def __init__( self ):
        self.creature = []
        self.item = []

    def render( self ):
        char = '.'
        for c in self.creature:
            char = c.render()
        return char
