if __name__ == "__main__":
    raise Exception('Do not run this file directly.')

class FloorRow():
    def __init__( self, floor, width=0 ):
        self.data = list((None for x in range( width )))
        self.floor = floor

    def grow( self, left, right ):
        for x in range(left):
            self.data.insert( 0, None )
        for x in range(right):
            self.data.append( None )

    def get_size( self ):
        return len(self.data)

    def range( self, minIdx, maxIdx ):
        for x in range( minIdx, maxIdx ):
            if x < 0 or x >= len(self.data):
                yield None
            else:
                yield self.data[x]

    def render( self, minIdx, maxIdx ):
        for x in self.range( minIdx, maxIdx ):
            if x is None:
                yield ' '
            else:
                yield x.render()


class Floor():
    def __init__( self ):
        self.xoff = 0
        self.yoff = 0
        self.row = []
        self.width = 0
        self.height = 0

    def render( self, vx, vy, vw, vh ):
        ret = []
        tx = vx - self.xoff
        for ry in range( vy, vy+vh ):
            ty = ry - self.yoff
            if ty < 0 or ty >= self.height:
                ret.append( ' ' * vw )
            else:
                ret.append(
                    ''.join( self.row[ty].render( tx, tx+vw ) )
                    )
        return ret

    def grow( self, left = 0, top = 0, right = 0, bottom = 0 ):
        if left < 0 or top < 0 or right < 0 or bottom < 0:
            raise Exception('Cannot grow by negative amounts.')

        self.width = self.width + left + right
        self.height = self.height + top + bottom
        self.xoff = self.xoff - left
        self.yoff = self.yoff - top
        for y in range(len(self.row)):
            self.row[y].grow( left, right )
        for y in range( top ):
            self.row.insert( 0, FloorRow( self, self.width ) )
        for y in range( bottom ):
            self.row.append( FloorRow( self, self.width ) )

    def is_in_bounds( self, x, y ):
        print('{},{} vs ({},{}) - ({}, {})'.format( x, y, self.xoff, self.yoff, self.width+self.xoff, self.height+self.yoff ) )
        if x < self.xoff or y < self.yoff:
            return False
        elif x >= self.width+self.xoff or y >= self.height+self.yoff:
            return False
        else:
            return True

    def get( self, x, y ):
        return self.row[y - self.yoff].data[x - self.xoff]

    def set( self, x, y, value ):
        self.row[y - self.yoff].data[x - self.xoff] = value

