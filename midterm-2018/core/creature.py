if __name__ == "__main__":
    raise Exception('Cannot run this file directly.')

class Creature:
    def __init__(self):
        # Base stats
        self.str = 10
        self.dex = 10
        self.int = 10
        self.wis = 10
        self.con = 10
        self.cha = 10
        self.hp = 5

        # Location
        self._floor = None
        self._x = None
        self._y = None

    def _clear_cell( self ):
        if self._floor is not None:
            cell = self._floor.get( self._x, self._y )
            if cell is not None:
                cell.creature.remove( self )
            self._floor = None
            self._x = None
            self._y = None

    def set_floor( self, floor, x, y ):
        self._clear_cell()
        self._floor = floor
        cell = self._floor.get( x, y )
        if cell is None:
            raise Exception('x & y are most likely off the edge of the map')
        cell.creature.append( self )
        self._x = x
        self._y = y
        
    def set_location( self, x, y ):
        self.set_floor( self._floor, x, y )

    def get_location( self ):
        return (self._floor, self._x, self._y)

    def render( self ):
        return '&'
