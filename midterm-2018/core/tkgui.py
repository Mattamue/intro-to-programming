import tkinter
import tkinter.font

from . import ui

class MainWnd( tkinter.Frame ):
    def __init__( self, parent, ui ):
        super().__init__( parent )
        self.map_width = 80
        self.map_height = 24
        self.pack()
        self.create_widgets()
        self.bind('<Key>', self.key_pressed )
        self.ui = ui
        self.focus_set()

    def create_widgets( self ):
        self.map_font = tkinter.font.Font(family='Courier', size=18)
        metrics = self.map_font.metrics()
        self.canvas = tkinter.Canvas(
            self,
            width = self.map_width * self.map_font.measure('M'),
            height = self.map_height * (metrics['linespace']-2)
            )

        self.text_row = []
        for row in range(0, self.map_height):
            self.text_row.append(
                self.canvas.create_text(
                    1,
                    # Accomidate for padding in the text widget
                    -1+(metrics['linespace']-2)*row,
                    font=self.map_font,
                    anchor='nw',
                    text=''
                    )
                )
        self.canvas.pack( side='left' )
        
        self.hi_there = tkinter.Button( self )
        self.hi_there['text'] = 'Hello'
        self.hi_there['command'] = self.say_hi
        self.hi_there.pack(side='top')

    def key_pressed( self, event ):
        if hasattr(event, 'char') and event.char != '':
            self.ui.on_input( event.char )
        else:
            self.ui.on_input( event.keysym )
    
    def say_hi( self ):
        self.canvas.itemconfig( self.txt, text='Whassup' );
        #self.canvas.insert( self.txt, 0, 'Hello, ' )

    def update_rows( self, rows ):
        for y in range(len(rows)):
            self.canvas.itemconfig( self.text_row[y], text=rows[y] )

class UiTk( ui.Ui ):
    def __init__( self, game_state ):
        self.game_state = game_state
        self.game_state.ui = self
        self.root = tkinter.Tk()
        self.wnd = MainWnd( self.root, self )
        
        self.view_x = 0
        self.view_y = 0

    def run( self ):
        self.update_display()
        self.wnd.mainloop()

    def on_input( self, key ):
        self.game_state.on_input( key )
        self.update_display()

    def exit( self ):
        self.root.destroy()

    def set_viewport_center( self, x, y ):
        self.view_x = x
        self.view_y = y

    def update_display( self ):
        x = self.view_x - int(self.wnd.map_width/2)
        y = self.view_y - int(self.wnd.map_height/2)
        self.wnd.update_rows(
            self.game_state.render( 
                x, y, self.wnd.map_width, self.wnd.map_height
                )
            )

