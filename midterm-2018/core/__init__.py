if __name__ == "__main__":
    raise Exception('Do not run this file directly.')

from .tkgui import UiTk as Ui
from .gamestate import GameState
