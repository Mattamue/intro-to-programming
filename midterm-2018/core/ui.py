if __name__ == "__main__":
    raise Exception('Do not run this file directly.')

class Ui:
    def __init__( self ):
        pass

    def run( self ):
        pass

    def on_input( self, key ):
        pass

    def exit( self ):
        pass

    def set_viewport_center( self, x, y ):
        pass

    def update_display( self ):
        pass
