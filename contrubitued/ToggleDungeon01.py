import tkinter as tk
from random import randint as roll

print('The dungeon is deep and said to be filled with treasures.')
print('Its upper levels have been explored by many;')
print('From its deeper levels, none has returned.')
print('A fearsome DRAGON is said to guard the deepest level.')
print('Will our brave hero be the first to plumb the depths and reach the TWENTIETH LEVEL?')
print('Will they flee rather than face mortal peril?')
print('Or will they perish, leaving no tale of their deeds?')

print('ACTIONS:')
print('FLEEING ends the game and reports a final score; our hero survives!')
print('RESTING allows the hero to recover from their wounds.')
print('SEARCHING gives a chance to find treasure.')
print('Taking time to search or rest increases the chances of meeting a wandering monster...')
print('...or a devious trap!')
print('')

print('FIGHTING: on meeting a monster, combat ensues.')
print('FLEEING requires that our hero fight one more round of combat before escaping.')
print('CHARGING lets you fight one round agressively, increasing your attack strength and damage.')
print('DEFENDING lets you fight one round defensively, increasing your defence and damage resistance.')
print('If you take no damage during a round, you regenerate some health.')
print('')

print('ENTER THE DUNGEON.')
print('')

heroName = input( "Please name our hero: " )

print("All hail the valiant {}.".format( heroName ) )


'''
Now I can create the tk window.
'''

frame = tk.Tk()


'''
Here follows the functions implementing the dungeon system
With print() statements removed in favour of state settings
and / or string return functions for use on labels
'''

def odds( up, down ):
    #return TRUE with odds of (up) out of (down), with safeties
    if up < 1 :
        return False  #no chance
    elif down < up :
        return True # certainty
    else:
        dice = roll(1,down)
        if dice > up:
            return False
        return True

def hit( a, b ):
    #return TRUE with odds of (a) against (b)
    #for CIV-style battle odds
    return odds( a, a+b )

def rollDamage( damage ):
    if damage < 1:
        return 1
    return roll( 1, damage )

class Creature: #superclass for hero and monsters
    def __init__(self, name):
        self.name=name #whoever
        self.attack=1 #to-hit
        self.defence=1 #not to be hit
        self.damage=1 #deal damage 1 to this
        self.maxhp=1 #max life
        self.hp=1 #hit points
        self.dr=0 #damage reduction (armor)
        self.regen=0 #hit point regain on rounds when NOT wounded!
        self.luck=0 #for heroes only
        #all these are placeholder values
        #luck is a hero stat affecting treasure find and monster avoid

    def isDead(self): #checker!
        if self.hp < 1:
            return True
        return False
    
    def isBloodied( self):
        if self.hp <= int( self.maxhp/2):
            return True
        return False

    def takeDamage(self, amount, flat = False): #modify hp based on incoming damage
        actual = amount
        #"flat" means the damage bypasses armor
        if not flat:
            actual = amount - self.dr
        if ( actual < 0 ):
            actual = 0
        self.hp -= actual #damage done

    def smoothHP(self): #for easy HP accountancy
        if self.hp > self.maxhp:
            self.hp = self.maxhp #do not exceed max

    def smoothStats(self): # in case things change #not needed yet
        #stats cannot drop below starting
        if self.attack < 1:
            self.attack = 1
        if self.defence < 1:
            self.defence = 1
        if self. damage < 1:
            self.damage = 1
        if self.maxhp < 1:
            self.maxhp = 1
        if self.dr < 0:
            self.dr = 0
        if self.regen < 0:
            self.regen = 0
        #maybe let luck go bad! :)

    def regain(self): #apply regen to hp if not dead
        #use this function in combat
        if self.isDead():
            return #nothing to do
        else:
            if self.hp < self.maxhp:
                self.hp += self.regen
                self.smoothHP()

    def restRegain(self): #apply healing and regen to hp if not dead
        #use this function out of combat
        if self.isDead():
            return #nothing to do
        else:
            if self.hp < self.maxhp:
                self.hp += self.regen + int(self.maxhp/5)
                self.smoothHP()

def tier1Name():
    s=[]
    s.append('Goblin')
    s.append('Orc')
    s.append('Raider')
    s.append('Gnoll')
    s.append('Kobold')
    q = roll(0, len(s)-1)
    return s[q]

def tier2Name():
    s=[]
    s.append('Troll')
    s.append('Ogre')
    s.append('Dark Elf')
    s.append('Fire Spirit')
    s.append('Zombie')
    q = roll(0, len(s)-1)
    return s[q]

def tier3Name():
    s=[]
    s.append('Wight')
    s.append('Vampire')
    s.append('Lich')
    s.append('Wraith')
    s.append('Banshee')
    q = roll(0, len(s)-1)
    return s[q]

def tier4Name():
    s=[]
    s.append('Manticore')
    s.append('Wyrm')
    s.append('Wyvern')
    s.append('Horror')
    s.append('Firedrake')
    q = roll(0, len(s)-1)
    return s[q]

def nameByTier( t ):
    if t < 1:
        t = 1
    elif t > 5:
        t = 5
    if t == 1:
        return tier1Name()
    elif t == 2:
        return tier2Name()
    elif t == 3:
        return tier3Name()
    elif t == 4:
        return tier4Name()
    else:
        return 'Dragon'


class Monster( Creature ):
    def __init__(self, level):
        super().__init__(" ") #create as a Creature with a blank name
        if level < 1:
            level = 1
        self.level = level
        self.tier = 1 #placeholder
        if level < 6:
            self.tier = 1
        elif level < 11:
            self.tier = 2
        elif level < 16:
            self.tier = 3
        elif level < 20:
            self.tier = 4
        else:
            self.tier = 5
        #print('{}: Level {} (Tier {})'.format( self.name, self.level, self.tier ))
        self.name = nameByTier( self.tier ) #auto-handled :)
        self.rollStats() #build my monster!

    #Monsters have a random role function which sets their stats
    #This function is based on level
    #Levels go by tiers: 1-5,6-10,11-15, 16-19, and 20 is special Tier 5
    
    def rollStats( self ):
        minStat = 1+self.tier
        maxStat = 2*minStat
        minLife = 1+self.level
        maxLife = 2*minLife
        #We'll keep dr and regen less than other stats
        self.attack = roll( minStat, maxStat )
        self.defence = roll( minStat, maxStat )
        self.damage = roll( minStat, maxStat )        
        self.maxhp = roll( minLife, maxLife )
        self.hp = self.maxhp
        #can't give them too much dr, regen or they are unkillable :)
        if self.tier < 2:
            self.dr = roll(0,1)
            self.regen = roll(0,1)
        else:
            self.dr = roll( 1,self.tier-1 )
            self.regen = roll( 0,self.tier -1 )
        
    def statString( self ):
        myString = '{} stats:'.format( self.name ) + "\n"
        myString += 'Attack {}  Defence {}  Damage {}'.format( self.attack, self.defence, self.damage ) + "\n"
        myString += 'HP: {} / {}  DR {}  Regen {}'.format( self.hp, self.maxhp, self.dr, self.regen )
        return myString
        
class Hero( Creature ):
    def __init__(self,name):
        super().__init__(name) #create as a Creature
        self.score=1
        self.xp = 0 # gain exp by winning
        self.treasure = 0 # items found
        self.level = 0 #how deep did we get in the dungeon?
        self.longname = self.name
        #Descriptors now
        self.mighty = False
        self.crafty = False
        self.cunning = False
        self.tough = False
        self.fierce = False
        self.lucky = False
        self.berserk = False
        #status to track when you run away
        self.flee = False #maybe drop in favour of toggle?
        #build here:
        self.setup()
        self.rollStats()


    def setup(self):
        #Placeholder values to begin with:
        self.attack = 5
        self.defence = 5
        self.damage = 10
        self.maxhp = 20
        self.hp = 20
        self.dr = 1
        self.regen = 1
        self.luck = 0

    def applyDescriptors(self):
        if self.mighty:
            self.longname += ", the Mighty"
            self.damage += 2
            self.maxhp += 5
            self.regen += 1
        if self.crafty:
            self.longname += ", the Crafty"
            self.attack += 2
            self.defence += 2
            self.luck += 1            
        if self.cunning:
            self.longname += ", the Cunning"
            self.luck += 1
            self.attack += 2
            self.damage += 2
        if self.tough:
            self.longname += ", the Tough"
            self.dr += 1
            self.maxhp += 5
            self.defence += 2
        if self.fierce:
            self.longname += ", the Fierce"
            self.regen += 1
            self.damage += 2
            self.attack += 2
        if self.lucky:
            self.longname += ", the Lucky"
            self.luck += 1
            self.defence += 2
            self.dr += 1
        if self.berserk:
            self.longname += ", the Berserk"
            self.dr += 1
            self.regen += 1
            self.maxhp += 5
        self.hp = self.maxhp

    def setDescriptor( self, d):
        if d == 1:
            self.mighty = True
        elif d == 2:
            self.crafty = True
        elif d == 3:
            self.cunning = True
        elif d == 4:
            self.tough = True
        elif d == 5:
            self.fierce = True
        elif d == 6:
            self.lucky = True
        elif d == 7:
            self.berserk = True

    def rollStats(self):
        #give the character some descriptors
        #first descriptor at random
        p = roll(1,7)
        #second descriptor different from first
        q = p
        while True:
            q = roll(1,7)
            if not q == p:
                break
        self.setDescriptor(p)
        self.setDescriptor(q)
        self.applyDescriptors()
        
    def recover( self ): #Hero function: recover after a battle
        #we will regain 20% hp plus regen
        if self.hp < self.maxhp:
            self.restRegain() 

    def statString( self ):
        myString = 'Stats for {}'.format( self.longname ) + ":\n"
        myString += 'Attack {}  Defence {}  Damage {}'.format( self.attack, self.defence, self.damage ) + "\n"
        myString += 'HP: {} / {}  DR {}  Regen {} Luck {}'.format( self.hp, self.maxhp, self.dr, self.regen, self.luck )
        return myString

#actually make our hero here :)
protag = Hero( heroName )
#and a placeholder monster
antag = Monster( 1 )

#now the treasures
class Treasure:
    def __init__(self, item, description, tag, value):
        self.item = item
        self.description = description
        self.tag = tag
        self.value = value #not used currently

    def aOfB(self): #generic name structure
        return '{} of {}'.format( self.item, self.description )

trove = [] #this will be a list of treasure items

def fillTrove( trove ):
    trove.clear()
    it = Treasure( 'Amethyst', 'Aggression', 'at', 1)
    trove.append( it )
    it = Treasure('Beryl', 'Blocking', 'dr', 1)
    trove.append( it )
    it = Treasure('Corundum', 'Concealment', 'df', 1)
    trove.append( it )    
    it = Treasure('Diamond', 'Defence', 'df', 1)
    trove.append( it )
    it = Treasure('Emerald', 'Energy', 'hp', 1)
    trove.append( it )
    it = Treasure('Fluorspar', 'Finding', 'lk', 1)
    trove.append( it )
    it = Treasure('Gem', 'Gentleness', 'hp', 1)
    trove.append( it )
    it = Treasure('Jewel', 'Jabbing', 'dm', 1)
    trove.append( it )
    it = Treasure('Lodestone', 'Luck', 'lk', 1)
    trove.append( it )
    it = Treasure('Opal', 'Offence', 'at', 1)
    trove.append( it )
    it = Treasure('Pearl', 'Protection', 'dr', 1)
    trove.append( it )
    it = Treasure('Ruby', 'Revival', 'rg', 1)
    trove.append( it )
    it = Treasure('Sapphire', 'Skewering', 'dm', 1)
    trove.append( it )
    it = Treasure('Topaz', 'Targetting', 'at', 1)
    trove.append( it )
    it = Treasure('Zircon', 'Zeal', 'rg', 1)
    trove.append( it )

def findTreasure( trove ):
    #new logic: just return a treasure item and replace it in the list with a Jar of Eyeballs :)
    q = roll(0, len(trove) - 1 ) #an entry
    it = Treasure('Jar', 'Eyeballs', 'nn', 0) #does nothing
    foo = trove[q] #a treasure item to return
    trove[q] = it #replace with Jar
    return foo

        
def applyBonus( buff, hero ): # apply the item buff to the hero:
    if buff.tag == 'at':
        hero.attack += buff.value
    elif buff.tag == 'df':
        hero.defence += buff.value
    elif buff.tag == 'dm':
        hero.damage += buff.value
    elif buff.tag == 'hp':
        hero.maxhp += 5*buff.value
        hero.hp += 5*buff.value
    elif buff.tag == 'dr':
        hero.dr += buff.value
    elif buff.tag == 'rg':
        hero.regen += buff.value
    elif buff.tag == 'lk':
        hero.luck += buff.value  


def randomTrapAttack( hero ):
    n = 5 #how many traps I have
    #maybe be cleverer later with lists
    q = roll(1,n)
    strikes = 0
    bypass = False #ignores armour
    damage = 0
    if q == 1:
        trapString = ' a vicious SPEAR TRAP! '
        strikes = 1
        damage = 15
    elif q == 2:
        trapString = ' a deadly SHOWER OF ARROWS! '
        strikes = 3
        damage = 8
    elif q == 3:
        trapString = ' a poisonous CHOKING GAS CLOUD! '
        strikes = 1
        damage = 10
        bypass = True
    elif q == 4:
        trapString = ' a hidden DEEP SPIKY PIT TRAP! '
        strikes = 1
        damage = 12
        bypass = True
    elif q == 5:
        trapString = ' a magical FIREBALL TRAP! '
        strikes = 4
        damage = 6
        bypass = True
    if strikes < 1:
        return #nothing to do
    scream = 'The search triggers:\n' + trapString
    #print( scream )
    while strikes > 0 and not hero.isDead():
        hpBefore = hero.hp
        strikes -= 1 #decrement
        ouch = rollDamage( damage )
        hero.takeDamage( ouch, bypass )
        hpAfter = hero.hp
        d = hpBefore - hpAfter
        if d > 0:
            scream += "\n {} damage done.".format( d )
    newMessage( LRep, scream)
        

def fightRound( hero, foe, charge, defend, fatigue ): #both are Creatures
    #charge, defend are Boolean toggled states
    hAttack = hero.attack
    hDefence = hero.defence
    fAttack = foe.attack
    fDefence = foe.defence
    hDamage = hero.damage
    fDamage = foe.damage
    if charge:
        hAttack *= 2 #berserk attack!
        hDamage *= 2
        hDefence = int( hDefence/2) #leaves you vulnerable
    if defend: #won't both be true, by calling logic
        hAttack = int( hAttack/2)
        hDefence *= 2
    hAttack -= fatigue #evil laughter
    hDefence -= fatigue
    hDamage -= fatigue
    hHit = hit( hAttack, fDefence )
    fHit = hit( fAttack, hDefence )
    if (hHit):
        #print('{} hit {}.'.format(hero.name, foe.name))
        dam = rollDamage( hDamage )
        foe.takeDamage( dam )
    if (fHit):
        #print('{} hit {}.'.format(foe.name, hero.name))
        dam = rollDamage( fDamage )
        hero.takeDamage( dam )     


'''
Below here are the toggle/button settings and logic to implement the clickable dungeon
'''

#States of the dungeon
exploring = True #initial state 
fighting = False # becomes True when monster encountered
searched = False # was the level searched?
rested = False # did the hero rest on this level?
charged = False # did the hero use Charge in this fight?
defended = False #did the hero use Defend in this fight?
bloodied = False #is the hero bloodied?
monsterBloodied = False # is the monster bloodied?

fled = False # becomes True when Flee is toggled
died = False # becomes True on death
won = False #becomes true on victory

level = 1 #your level in the dungeon, increments during game

rounds = 0 # rounds of fighting!

empty = True # becomes False when inventory is not empty
inventory = [] # a list of thing you have
fillTrove( trove ) #set up the hoard

#some handler functions for labels and buttons


def newMessage( thing, words ):
    thing.configure( text=words ) #works on labels and buttons :)
    return

def newColour( thing, fore, back ):
    thing.configure( fg = fore , bg = back )
    return

def greyOut( thing ):
    newColour( thing, "black", "lightgrey" )
    return

def whiteBlack( thing):
    newColour( thing, "black", "white" )
    return

def redOut( thing ):
    newColour( thing, "black", "red" )
    return

def resetState():
    global exploring
    exploring = True #initial state
    global fighting
    fighting = False # becomes True when monster encountered
    global searched
    searched = False # was the level searched?
    global rested
    rested = False # did the hero rest on this level?
    global charged
    charged = False # did the hero use Charge in this fight?
    global defended
    defended = False #did the hero use Defend in this fight?
    global bloodied
    bloodied = False #is the hero bloodied?
    global monsterBloodied
    monsterBloodied = False # is the monster bloodied?

    global fled
    fled = False # becomes True when Flee is toggled
    global died
    died = False # becomes True on death
    global won
    won = False # true on victory

    global protag
    protag = Hero( heroName ) #new hero
    antag = Monster( 1 ) #new monster

    global level
    level = 1 #your level in the dungeon, increments during game

    global empty
    empty = True # becomes False when inventory is not empty

    global inventory
    inventory = [] # a list of thing you have
    fillTrove( trove ) #reset the hoard
    newMessage( LMonster, "Monster info will appear here\n When you meet one, anyway." )



#now some variables toggling which buttons are active and which not
showProceed = True #True while exploring unless fled or died
showSearch = True # True while exploring unless fled or died or searched
showRest = True # ditto for rested
showFight = False #True when fighting, unless etc.
showCharge = False # True when fighting unless etc. or charged
showDefend = False # likewise for defended
showFlee = True # True unless fled or died
showReset = True #always True
showQuit = True # always True

#Note: if a button's show is False, clicking it returns immediately
#so that's the first line of each bind-to-event routine :)  

#now a function which sets the showX variables based on the dungeon state
def updateShow():
    #I need global for variables I might change...
    global showProceed
    global showSearch
    global showRest
    global showFight
    global showCharge
    global showDefend
    global showFlee
    global bloodied
    bloodied = protag.isBloodied()
    global monsterBloodied
    monsterBloodied = antag.isBloodied()
    #I don't need global for variables I only USE but what the hell
    global fled, died, won, searched, rested, charged, defended, fighting
    itsOver = fled or died or won
    showProceed = True
    showSearch = True
    showRest = True
    if itsOver:
        showProceed  = False
        showSearch = False
        showRest = False
    if searched:
        showSearch = False
    if rested:
        showRest = False
    showFight = False
    showCharge = False
    showDefend = False
    if fighting:
        showRest = False
        showSearch = False
        showProceed = False
        showFight = True
        showCharge = True
        showDefend = True
    if charged:
        showCharge = False
    if defended:
        showDefend = False
    showFlee = True
    if itsOver:
        showFlee = False
    return

'''
Herewith a list of labels and buttons for the window

Top label - banner with game name LTop
Label - dungeon level and score LScore
Label - stat block with stats LStat
Label - inventory with contents LInv

Button - Proceed (also Continue) BProc
Button - Search BSearch
Button - Rest BRest
Button - Fight BFight
Button - Charge! BCharge
Button - Defend BDef

Label - Report (latest dungeon event) LRep

Label - Monster stats or blank LMonster

Button - FLEE  BFlee
Button - RESET  BReset
Button - QUIT  BQuit


'''

#I'll define some functions for the label entry strings

def LTopString():
    return "----------Welcome to Toggle Dungeon!----------"

def LScoreString():
    return "On level: {}".format( level ) #There is no score, only level. And Zuul.

def LStatString():
    return protag.statString() #class function :)

def LInvString():
    x = "Inventory:"
    items = 0
    if inventory:
        for y in inventory:
            x += "\n" + y.aOfB() #adds a treasure name to the string
            items += 1
    else:
        x = x + "\n [empty]"
    if items > 15:
        x = "Inventory:\n Too full to list."
    return x

def LRepString():
    return "\n Reports will appear here. \n "

def LMonsterString():
    return "Monster info will appear here\n When you meet one, anyway."



LTop = tk.Label( frame )
LTop.pack( fill = 'x' )
newMessage( LTop, LTopString()  )

LScore = tk.Label( frame )
LScore.pack( fill = 'x' )
newMessage( LScore, LScoreString()  )

LStat = tk.Label( frame )
LStat.pack( fill = 'x' )
newMessage( LStat, LStatString() )

LInv = tk.Label( frame )
LInv.pack( fill = 'x' )
newMessage( LInv, LInvString() )

BProc = tk.Button( frame )
BProc.pack()
newMessage( BProc, "Proceed..." )

BSearch = tk.Button( frame )
BSearch.pack()
newMessage( BSearch, "Search for treasure" )

BRest = tk.Button( frame )
BRest.pack()
newMessage( BRest, "Rest and recover" )

BFight = tk.Button( frame )
BFight.pack()
newMessage( BFight, "Fight!" )

BCharge = tk.Button( frame )
BCharge.pack()
newMessage( BCharge, "Charge!" )

BDefend = tk.Button( frame )
BDefend.pack()
newMessage( BDefend, "Defend!" )

LRep = tk.Label( frame )
LRep.pack( fill = 'x' )
newMessage( LRep, LRepString() )

LMonster = tk.Label( frame )
LMonster.pack( fill = 'x' )
newMessage( LMonster, LMonsterString() )

BFlee = tk.Button( frame )
BFlee.pack()
newMessage( BFlee, "Flee! (End game)" )


BReset = tk.Button( frame )
BReset.pack()
newMessage( BReset, "Reset (Restart game)" )

BQuit = tk.Button( frame )
BQuit.pack()
newMessage( BQuit, "Quit (close window)" )

#a function to control button and label appearance by variables
def updateScreen():
    #the relevant variables should all be accessible
    if showProceed:
        whiteBlack( BProc )
    else:
        greyOut( BProc )
    if showSearch:
        whiteBlack( BSearch )
    else:
        greyOut( BSearch )
    if showRest:
        whiteBlack( BRest )
    else:
        greyOut( BRest)
    if showFight:
        whiteBlack( BFight )
    else:
        greyOut( BFight )
    if showCharge:
        whiteBlack( BCharge )
    else:
        greyOut( BCharge )
    if showDefend:
        whiteBlack( BDefend )
    else:
        greyOut( BDefend )
    if showFlee:
        whiteBlack( BFlee )
    else:
        greyOut( BFlee )
    if bloodied:
        redOut( LStat )
    elif died:
        greyOut( LStat )
    else:
        whiteBlack( LStat )
    whiteBlack( LRep )
    if monsterBloodied:
        redOut( LMonster )
    else:
        whiteBlack( LMonster )
    whiteBlack( BReset )
    whiteBlack( BQuit )
    newMessage( LTop, LTopString()  )
    newMessage( LScore, LScoreString()  )
    newMessage( LStat, LStatString() )
    newMessage( LInv, LInvString() )
    frame.lift()
    frame.attributes("-topmost", True)
    return


updateShow()
updateScreen()

#quitting is the easiest thing to handle :)
def quitGame( event ):
    frame.destroy()

def resetGame( event ):
    resetState()
    newMessage( LRep, "The eternal quest begins anew." )
    updateShow()
    updateScreen()    

BQuit.bind("<Button-1>", quitGame )
BReset.bind("<Button-1>", resetGame )

#introduce fled as a simple toggle?
#have the fled button toggle fled state and return
#handle the fled state inside exploring or fighting

def fledMessage():
    theMessage = "{} abandons the quest.\n".format( protag.name )
    theMessage += "They reached dungeon level {}\n".format( level )
    theMessage += "and found {} treasures.".format( str(len(inventory) ) )
    newMessage( LRep, theMessage )
    updateShow()
    updateScreen()    

def fledBattleMessage():
    theMessage = "{} tries to flee...\n".format( protag.name )
    theMessage += "...but must fight their way free!"
    newMessage( LRep, theMessage )
    updateShow()
    updateScreen() 

def setFlee( event ):
    global fled
    if fled:
        return #don't do it twice :)
    fled = True
    exploring = False
    #handling now depends on whether we are fighting or not!
    if fighting:
        fledBattleMessage()
        return # Battle routine to handle this
    else:
        fledMessage()

BFlee.bind("<Button-1>", setFlee )



def doRest(event):
    global rested
    if fighting:
        return
    if rested:
        return
    if fled:
        return
    if not exploring:
        return
    else:
        rested = True
        statusLine = "{} rests a while.\n It's refreshing.".format( protag.name )
        hpBefore = protag.hp
        protag.recover()
        hpAfter = protag.hp
        d = hpAfter - hpBefore
        if d > 0:
            statusLine += "\n {} hit points recovered.".format( d  )
        newMessage( LRep, statusLine )
        updateShow()
        updateScreen()
        
BRest.bind("<Button-1>", doRest)

def doSearch(event):
    global searched
    global protag
    global died
    global exploring
    global fled
    if fled:
        return
    if fighting:
        return
    if searched:
        return
    if not exploring:
        return
    else:
        searched = True
        statusLine = "The hero searches for treasure...\n"
        #new three-way logic
        #find NOTHING or TREASURE or TRAP
        #roll a d30
        #find TREASURE on (10+luck) or less
        #find TRAP on more than 20+luck
        x = roll(1,30)
        if x < ( 11 + protag.luck) :
            booty = findTreasure( trove )
            applyBonus( booty, protag )
            inventory.append( booty ) #an item from the hoard is now yours!
            statusLine += "...and finds something!\n"
            statusLine += "**" + booty.aOfB() + "**"
            newMessage( LRep, statusLine )
        elif x > (20 + protag.luck) :
            randomTrapAttack( protag )
            if protag.isDead():
                died = True
                exploring = False
                deathString = "{} died in a trap.\n None shall ever know their story.".format(protag.name)
                newMessage( LRep, deathString )
        else:
            statusLine += "...but finds nothing.\n Such is life."
            newMessage( LRep, statusLine )  
        updateShow()
        updateScreen()
        
BSearch.bind("<Button-1>", doSearch)


def doProceed( event ):
    global searched, rested, level, charged, defended, fighting, rounds
    final = False #becomes true on final (20th) level
    if fled:
        return
    if fighting:
        return #only active if not fighting!
    if not exploring:
        return
    else:
        statusLine = "Proceeding through the dungeon...\n"
        #do a monster check here
        encounter = False
        if level == 20:
            encounter = True
            final = True
        else:
            #odds of finding depend on luck, searched, rested
            #without luck: 1/3 chance if wasted no time
            #2/3 chance if searched and rested
            #1/2 chance if one or the other
            numerator = 10 - protag.luck
            if searched:
                numerator += 5
            if rested:
                numerator += 5
            denom = 30
            encounter = odds( numerator, denom )
        if encounter:
            #do an encounter
            statusLine += "{} encounters a fell opponent!".format(heroName)
            if final:
                statusLine += "\n It is the fabled DRAGON! The final battle..."
        else:
            statusLine = statusLine + "{} finds a safe pathway onwards...".format(heroName)
        newMessage( LRep, statusLine )
        #if an encounter, do fight here
        #generate the monster inside doBattle! - my creation logic handles the dragon :)
        if encounter:
            startBattle() #create a monster!
            fighting = True
            rounds = 0
            return #this is going to be some interesting logic!
        else:
            goToNextLevel()

def goToNextLevel():
    global level, searched, rested, charged, defended, fighting, exploring
    global antag
    level = level + 1
    searched = False
    rested = False
    charged = False
    defended = False
    fighting = False
    exploring = True
    antag = Monster( 1 )
    newMessage( LMonster, "\n\n" ) #blank the Monster screen!
    updateShow() #includes new level report
    updateScreen()

BProc.bind("<Button-1>", doProceed)


def startBattle():
    #we got here from Proceed
    #set the buttons toggles for fighting status
    global fighting, charged, defended, fled, died, won, bloodied, monsterBloodied, antag
    fighting = True
    charged = False
    defended = False
    fled = False #must be true when we start...
    died = False
    won = False
    #don't update bloodied because might be carrying a wound!
    monsterBloodied = False #maybe set using Creature flags later
    #create a monster using the usual creation logic
    antag = Monster(level)
    newMessage( LMonster, antag.statString() )
    updateShow()
    updateScreen()


#now have a function bound to Fight!
#which, if we are fighting
#fights one round of the battle between hero and antag.
#track both their hp to find out if they regenerate!
#then check on global status:
#if the hero died then do the Died thing
#if the monster died, do the next level thing
#unless it was the last level, in which case do the victory thing
#if the monster is alive but the hero fled,
#do the fledMessage thing


def winMessage():
    theMessage = "The final foe lies slain!\n"
    theMessage += "{} has completed their quest!\n".format( protag.name )
    theMessage += "They reached dungeon level {}\n".format( level )
    theMessage += "and found {} treasures.\n\n".format( str(len(inventory) ) )
    theMessage += "Honour and glory to {}!\n".format( protag.longname )
    newMessage( LRep, theMessage )
    updateShow()
    updateScreen() 

def doFight( event ):
    global died, fighting, exploring, fled, rounds
    global protag, antag
    if not fighting:
        return
    if died:
        return #to inactivate

    heroHP = protag.hp
    foeHP = antag.hp
    heroHurt = False
    foeHurt = False
    rounds += 1

    fightString = "Clashing in battle... ( round {} )\n".format( str( rounds ) )

    #the evil bit: enforce fatigue penalty :)
    fatigue = int( rounds / protag.maxhp )

    fightRound( protag, antag, False, False, fatigue )

    #handle some healing here for those not dead yet
    if heroHP > protag.hp:
        heroHurt = True
    if foeHP > antag.hp:
        foeHurt = True

    if not heroHurt:
        fightString += "The hero is unscathed!\n"
        protag.regain() #regenerate some health
    else:
        d = heroHP - protag.hp
        fightString += "The foe rends the hero for {} damage.\n".format( str(d) )
    if not foeHurt:
        fightString += "The foe is unscathed!\n"
        antag.regain() #regenerate some health
    else:
        d = foeHP - antag.hp
        fightString += "The hero smites the foe for {} damage.\n".format( str(d) )

    newMessage( LMonster, antag.statString() )
    newMessage( LStat, protag.statString() )

    reportFightResult( fightString ) #pass on to the generic results function
    #this makes it easier to handle charge and defend!

BFight.bind("<Button-1>", doFight)

def reportFightResult( fightString ):
    global protag, antag, died, fighting, exploring, fled, level
    #now the checks!
    if protag.isDead():
        died = True
        fighting = False
        exploring = False
        deathString = "{} died in battle.\n None shall ever know their story.".format(protag.name)
        fightString += deathString
        newMessage( LRep, fightString )
        updateShow()
        updateScreen()
        
        return #and we're done :)
    elif fled:
        fighting = False
        exploring = False
        fledMessage()
        updateShow()
        updateScreen()
        return #quit, even if foe was slain, you coward.
    elif antag.isDead():
        if level == 20: #final battle won!
            won = True
            fighting = False
            exploring = False
            winMessage()
            return
        else:
            fightString += "The foe lies slain!\n"
            fightString += "The hero advances through the dungeon."
            fighting = False
            exploring = True
            newMessage( LRep, fightString )
            goToNextLevel()
            return
    else: #battle continues!
        fightString += "The battle continues..."
        newMessage( LRep, fightString )
        updateShow()
        updateScreen()
        return


#future rewrite plan: use some other kind of state function for charging, defending
#so that one function not three almost identical ones
#but this worke for now...


def doCharge( event ):
    global died, fighting, exploring, fled, rounds, charged
    global protag, antag
    if not fighting:
        return
    if died:
        return #to inactivate
    if charged:
        return #only once per battle!
    charged = True #mark!

    heroHP = protag.hp
    foeHP = antag.hp
    heroHurt = False
    foeHurt = False
    rounds += 1

    fightString = "Charging into battle... ( round {} )\n".format( str( rounds ) )
    #the evil bit: enforce fatigue penalty :)
    fatigue = int( rounds / protag.maxhp )
    fightRound( protag, antag, True, False, fatigue )

    #handle some healing here for those not dead yet
    if heroHP > protag.hp:
        heroHurt = True
    if foeHP > antag.hp:
        foeHurt = True

    if not heroHurt:
        fightString += "The hero is unscathed!\n"
        protag.regain() #regenerate some health
    else:
        d = heroHP - protag.hp
        fightString += "The foe rends the hero for {} damage.\n".format( str(d) )
    if not foeHurt:
        fightString += "The foe is unscathed!\n"
        antag.regain() #regenerate some health
    else:
        d = foeHP - antag.hp
        fightString += "The hero smites the foe for {} damage.\n".format( str(d) )

    newMessage( LMonster, antag.statString() )
    newMessage( LStat, protag.statString() )

    reportFightResult( fightString ) #pass on to the generic results function

BCharge.bind("<Button-1>", doCharge)
    
def doDefend( event ):
    global died, fighting, exploring, fled, rounds, defended
    global protag, antag
    if not fighting:
        return
    if died:
        return #to inactivate
    if defended:
        return #only once per battle!
    defended = True


    heroHP = protag.hp
    foeHP = antag.hp
    heroHurt = False
    foeHurt = False
    rounds += 1

    fightString = "Going cautiously into battle... ( round {} )\n".format( str( rounds ) )
    #the evil bit: enforce fatigue penalty :)
    fatigue = int( rounds / protag.maxhp )
    fightRound( protag, antag, False, True, fatigue )

    #handle some healing here for those not dead yet
    if heroHP > protag.hp:
        heroHurt = True
    if foeHP > antag.hp:
        foeHurt = True

    if not heroHurt:
        fightString += "The hero is unscathed!\n"
        protag.regain() #regenerate some health
    else:
        d = heroHP - protag.hp
        fightString += "The foe rends the hero for {} damage.\n".format( str(d) )
    if not foeHurt:
        fightString += "The foe is unscathed!\n"
        antag.regain() #regenerate some health
    else:
        d = foeHP - antag.hp
        fightString += "The hero smites the foe for {} damage.\n".format( str(d) )

    newMessage( LMonster, antag.statString() )
    newMessage( LStat, protag.statString() )

    reportFightResult( fightString ) #pass on to the generic results function

BDefend.bind("<Button-1>", doDefend)



frame.mainloop()
