#!/usr/bin/env python3

import random
import csv

class Namegen:
    '''Simple class that loads some datafiles and then puts together random
    names that sorta' look like real names.'''
    def __init__(self):
        # Load up first names, this one comes from the census and has a bunch
        # of really neat statistical data we're ignoring.  So, we split the
        # lines in order to get the first word
        # I got the list here:
        # http://deron.meranda.us/data/census-derived-all-first.txt
        self.first = []
        with open('datafiles/census-derived-all-first.txt', 'r') as fin:
            for line in fin:
                chunks = line.split(' ')
                if chunks is not None and len(chunks) > 0:
                    self.first.append( chunks[0].title() )

        # Load up surnames, this is a flat file I got here:
        # https://www.scrapmaker.com/data/wordlists/names/surnames.txt
        self.last = []
        with open('datafiles/surnames.txt', 'r') as fin:
            for line in fin:
                line = line.strip()
                # Skip blank lines and headers (this file has alphabetical,
                # single character headers)
                if len(line) <= 1:
                    continue
                self.last.append( line.title() )

        # Nouns?!  I got the list here: http://www.desiquintans.com/nounlist
        self.noun = []
        with open('datafiles/nounlist.txt', 'r') as fin:
            for line in fin:
                line = line.strip()
                if len(line) <= 0:
                    continue
                self.noun.append( line.title() )

    def gen( self ):
        norm = 3
        count = 0
        for j in range(norm):
            count = count + random.randint(1,3)
        count = int(count / norm)
        name = []
        for j in range(count):
            name.append( random.choice(self.first) );
        name.append( random.choice(self.last) )
        
        # Randomly include a nickname, currently 5% of the time
        if random.randint(1, 100) <= 5:
            name.insert( 1, '"' + random.choice(self.noun) + '"' );

        return ' '.join( name )

# Create a namer instance
namer = Namegen()

# Create a new "import" file and filter list
with open('import.csv', 'w') as fout, open('filter.txt', 'w') as ffilt:
    cout = csv.writer( fout )
    # Generate 100,000 records
    for x in range( 0, 100000 ):
        email = 'email.address.{:0>6}@example.com'.format(x)
        name = namer.gen()
        cout.writerow( (
            email,
            name,
            str(random.randint(100,1000)),
            str(random.randint(100,1000)),
            str(random.randint(100,1000)),
            ) )
        # Pick 1% of records to populate the filter list.
        if random.randint(1, 100) <= 1:
            ffilt.write( email + '\n' )

