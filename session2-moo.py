#!/usr/bin/env python3

import random

referenceCode = "abc"

while True:
    code = referenceCode
    guess = input('Enter your guess: ')

    # Check the length of the guess
    if len(code) != len(guess):
        print('Guess is the wrong length, enter %d letters.' % len(code) )
        continue

    # Check for success
    if code == guess:
        print('You guessed correctly!')
        break

    # Check for correct answers in correct locations
    correct = 0
    misplaced = 0
    for index in range(0, len(code)):
        if code[index] == guess[index]:
            # the letter is completely correct
            correct = correct + 1
            code = code[:index] + '-' + code[index+1:]
            guess = guess[:index] + '-' + guess[index+1:]

    for letter in guess:
        if letter == '-':
            continue
        else:
            # Is the letter in the wrong place?
            index = code.find( letter )
            if index < 0:
                print('Letter %c not found' % letter )
            else:
                print('Letter %c found at position %d' % (letter, index) )
                misplaced = misplaced + 1
                code = code[:index] + '-' + code[index+1:]
            

    # Provide feedback
    print('You have %d letters correct, and %d misplaced' % (correct, misplaced) )
    
